from .base import *

SECRET_KEY = env('SECRET_KEY', default='change-me')
DEBUG = env('DEBUG', default='True')
ALLOWED_HOTS = ['*']
