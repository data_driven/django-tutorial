from .base import *

SECRET_KEY = env('SECRET_KEY') # default 옵션을 주지않으면 해당 환경변수 없을 때, 에러발생
DEBUG = env('DEBUG', default='False')
ALLOWED_HOTS = [
    '.example.com',
]
